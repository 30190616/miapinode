var mymap = L.map('map').setView([7.8882092,-72.4992668], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(mymap);

/*var marker = L.marker([7.8882092,-72.4992668]).addTo(mymap);
var marker = L.marker([7.894309, -72.501611]).addTo(mymap);
var marker = L.marker([7.897764, -72.488740]).addTo(mymap);*/

$.ajax({
    dataType: "json",
    url: 'api/motos',
    success: function(resultado) {
        console.log(resultado);
        resultado.motos.forEach(function(moto) {
            L.marker(moto.ubicacion).addTo(mymap);
        });
    }
})