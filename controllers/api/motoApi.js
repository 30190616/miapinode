var Moto = require('../../models/motoModel');

exports.moto_lista_get = function(req, res) {
    
    res.status(200).json({
        motos: Moto.listaMotos
    })
};

exports.moto_crear_post = function(req, res) {
    
    var moto = new Moto(req.body.id, req.body.color, req.body.modelo);
    moto.ubicacion = [req.body.latitud, req.body.longitud];

    Moto.agregar(moto);

    res.status(201).json({
        moto: moto
    });
};

exports.moto_actulizar_patch = function(req, res) {
    
    var moto = Moto.buscar(req.body.id);
    moto.color = req.body.color;
    moto.modelo = req.body.modelo;
    moto.ubicacion = [req.body.latitud, req.body.longitud];

    res.status(200).json({
        moto: moto
    });
};

exports.moto_eliminar_delete = function(req, res) {

    Moto.eliminar(req.body.id);
    res.status(204).send();
};