var express = require('express');
var router = express.Router();
var motoApi = require('../../controllers/api/motoApi');

router.get('/', motoApi.moto_lista_get);
router.post('/crear', motoApi.moto_crear_post);
router.patch('/actualizar', motoApi.moto_actulizar_patch);
router.delete('/eliminar', motoApi.moto_eliminar_delete);

module.exports = router;