var express = require('express');
var router = express.Router();
var motosController = require('../controllers/motosController')

router.get('/', motosController.moto_lista);
router.get('/crear', motosController.moto_crear_get);
router.post('/crear', motosController.moto_crear_post);
router.get('/:id/actualizar', motosController.moto_actualizar_get);
router.post('/:id/actualizar', motosController.moto_actualizar_post);
router.post('/:id/eliminar', motosController.moto_eliminar_post);

module.exports = router;