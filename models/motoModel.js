var Moto = function(id, color, modelo, ubicacion) {

    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
};

Moto.listaMotos=[];

Moto.agregar = function(moto) {
    Moto.listaMotos.push(moto)
};

Moto.buscar= function(motoId) {
    var motoId = Moto.listaMotos.find(x => x.id == motoId);

    if (motoId) {
        return motoId
    } else {
        throw new Error(`No existe una moto con el id ${motoId}`)
    }
};

Moto.eliminar = function(motoId) {
    for (var i = 0; i < Moto.listaMotos.length; i++) {
        if(Moto.listaMotos[i].id == motoId) {
            Moto.listaMotos.splice(i, 1);
            break;
        }
        
    }
};

var moto1 = new Moto(1, "azul", "yamaha", [7.899007, -72.486685]);
var moto2 = new Moto(2, "negro", "suzuki", [7.911483, -72.503830]);
var moto3 = new Moto(3, "gris", "honda", [7.914319, -72.469164]);

Moto.agregar(moto1);
Moto.agregar(moto2);
Moto.agregar(moto3);

module.exports = Moto;